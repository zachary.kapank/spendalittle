import React, { useEffect } from 'react';
import { useProduct } from '../../contexts/product.context';
import { useCart } from '../../contexts/cart.context';
import { useParams } from 'react-router-dom';
import Display from './display/display';

export default () => {
	const { productId } = useParams();
	const { product, getProduct } = useProduct();
	const { addItemToCart } = useCart();

	useEffect(() => {
		getProduct(parseInt(productId));
	}, [getProduct, productId]);

	if (!product) return <div>no product</div>;

	return <Display {...product} addItemToCart={addItemToCart} />;
};
