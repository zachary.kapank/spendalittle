import styled from "styled-components";
import { Button } from "../../../components";

export const Wrapper = styled.div`
  width: 100%;
  margin-top: 25px;
  display: flex;
  flex-direction: column;
  align-items: center;
  min-height: 110vh;
`;

export const ProductDisplay = styled.div`
  width: 80%;
  display: flex;
  flex-direction: row;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 100%;
  }
`;

export const ImageBox = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  padding: 25px;
  margin-right: 30px;
  -webkit-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0px 0px 13px 3px ${({ theme }) => theme.palette.cardBorderColor};

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    flex-direction: column;
    margin: 0 10px;
  }
`;

export const ActionBox = styled.div`
  width: 300px;
  height: 150px;
  display: inline-flex;
  flex-direction: column;
  padding: 15px;

  -webkit-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0px 0px 13px 3px ${({ theme }) => theme.palette.cardBorderColor};

  span {
    font-size: 0.8rem;
    font-weight: 500;
    color: ${({ theme }) => theme.palette.textColorDark};
  }

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 100%;
    height: auto;
    display: flex;
    flex-direction: row;
    padding: 25px;
    margin: 0;
    position: fixed;
    bottom: 0;
    background: #fff;

    button {
      width: 100%;
    }
  }
`;

export const ProductImage = styled.img`
  width: 300px;
  height: 300px;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 100%;
  }
`;

export const ProductMeta = styled.div`
  width: 100%;
  margin: 0 10px;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    margin: 10px 0px;
  }
`;

export const ProductStock = styled.div`
  font-size: 1rem;
  color: ${({ theme }) => theme.palette.textColorDark};
`;

export const ProductTitle = styled.div`
  font-size: 1.4rem;
  font-weight: 600;
  margin: 10px 0;
`;

export const ProductPrice = styled.div`
  font-size: 1.6rem;
  font-weight: 500;
  margin: 1rem 0;
  color: ${({ theme }) => theme.palette.secondary};
`;

export const ProductDescription = styled.div`
  width: 80%;
  padding: 25px;
  -webkit-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0px 0px 13px 3px ${({ theme }) => theme.palette.cardBorderColor};

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 95%;
    margin-bottom: 200px;
  }
`;

export const Header = styled.div`
  font-size: 1.2rem;
  font-weight: 600;
  padding: 10px 0;
  color: ${({ theme }) => theme.palette.secondary};
  border-bottom: 2px solid ${({ theme }) => theme.palette.secondary};
  margin-bottom: 25px;
`;

export const AddToCart = styled(Button)``;
