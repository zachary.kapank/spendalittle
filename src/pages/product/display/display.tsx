import React from "react";
import {
  Wrapper,
  ProductDisplay,
  ImageBox,
  ActionBox,
  ProductImage,
  ProductMeta,
  ProductTitle,
  ProductDescription,
  ProductPrice,
  ProductStock,
  Header
} from "./style";
import { Spacer, Button } from "../../../components";

export default ({
  id,
  name,
  image,
  description,
  currency,
  unitPrice,
  isAvailable,
  addItemToCart
}: any) => {
  return (
    <Wrapper>
      <ProductDisplay>
        <ImageBox>
          <ProductImage src={image} alt={name} />
          <ProductMeta>
            <ProductTitle>{name}</ProductTitle>
            <ProductStock>
              {isAvailable ? "in stock" : "out of stock"}
            </ProductStock>
          </ProductMeta>
        </ImageBox>

        <ActionBox>
          <ProductPrice>
            {currency} {unitPrice}
          </ProductPrice>
          <Spacer space={10} />
          <Button
            color="secondary"
            onClick={() =>
              addItemToCart({
                id,
                qty: 1,
                unitPrice,
                image,
                name,
                isAvailable,
                currency
              })
            }
          >
            add to cart
          </Button>
        </ActionBox>
      </ProductDisplay>
      <Spacer space={50} />
      <ProductDescription>
        <Header>Description</Header>
        {description}
      </ProductDescription>
    </Wrapper>
  );
};
