import React, { useState, useEffect } from "react";
import ReactPaginate from "react-paginate";
import { useCart } from "../../contexts/cart.context";
import { useProduct } from "../../contexts/product.context";
import { Wrapper } from "./styles";
import Search from "./Search/search";
import Products from "./ProductList/productList";

export default () => {
  const { addItemToCart } = useCart();
  const { products, listProducts } = useProduct();
  const [offset, setOffset] = useState(0);
  const perPage = 7;

  useEffect(() => {
    listProducts(offset, perPage);
  }, [listProducts, offset]);

  const handlePageClick = (data: any) => {
    let selected = data.selected;
    let offset = Math.ceil(selected * perPage);

    setOffset(offset);

    listProducts(offset, perPage);
  };

  return (
    <Wrapper>
      <Search handleChange={listProducts} offset={offset} perPage={perPage} />
      <Products
        products={products}
        addItemToCart={addItemToCart}
        listProducts={listProducts}
      />
      <ReactPaginate
        containerClassName={"paginate"}
        pageClassName={"page"}
        activeLinkClassName={"activePagination"}
        previousClassName={"previous"}
        nextClassName={"next"}
        pageCount={10 / 7}
        onPageChange={handlePageClick}
        pageRangeDisplayed={5}
        marginPagesDisplayed={2}
      />
    </Wrapper>
  );
};
