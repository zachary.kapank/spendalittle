import React from "react";
import { SearchInput, Wrapper } from "./styles";

export default ({ handleChange, offset, perPage }: any) => {
  return (
    <Wrapper>
      <SearchInput
        placeholder="Search for a product"
        onChange={e => handleChange(offset, perPage, e.target.value)}
        type="text"
      />
    </Wrapper>
  );
};
