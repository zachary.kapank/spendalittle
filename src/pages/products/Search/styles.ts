import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px 0;
  background: ${({ theme }) => theme.palette.secondary};

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    padding: 15px;
  }
`;

export const SearchInput = styled.input`
  width: 600px;
  padding: 10px 28px 10px 14px;
  border-radius: 5px;
  border: none;
  font-size: 0.9rem;
  color: ${({ theme }) => theme.palette.textColorDark};

  &:focus {
    outline: none;
  }

  &::placeholder {
    color: ${({ theme }) => theme.palette.textColorDark};
    font-size: 1rem;
    opacity: 0.9;
  }

  &.table {
    border: 1.5px solid ${p => p.theme.secondaryColor};
    margin: 10px 0;
  }
`;
