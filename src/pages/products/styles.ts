import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Products = styled.div`
  width: 60%;
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    bottom: 0;
    width: 100%;
  }
`;

export const Product = styled.div`
  margin: 15px;
  display: flex;
  padding: 25px;
  background: #fff;
  -webkit-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0px 0px 13px 3px ${({ theme }) => theme.palette.cardBorderColor};

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    flex-direction: column;
  }
`;

export const ProductImage = styled.img`
  width: 150px;
  height: 150px;
  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 100%;
    height: 200px;
  }
`;

export const ProductTitle = styled.div`
  font-size: 1.4rem;
  font-weight: 600;
  margin: 1rem 0;
`;

export const ProductPrice = styled.div`
  font-size: 1.6rem;
  font-weight: 500;
  margin: 1rem 0;
  color: ${({ theme }) => theme.palette.secondary};
`;

export const ProductStock = styled.div`
  font-size: 1rem;
  color: ${({ theme }) => theme.palette.textColorDark};
`;

export const ProductInformation = styled.div``;

export const ProductMeta = styled.div`
  width: 100%;
  margin: 0 15px;
  display: flex;
  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

export const ProductActions = styled.div`
  width: 300px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 100%;
    margin: 5px 0;
  }
`;
