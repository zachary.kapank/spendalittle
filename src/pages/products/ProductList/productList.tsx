import React from "react";
import { Link } from "react-router-dom";

import {
  Products,
  Product,
  ProductImage,
  ProductMeta,
  ProductActions,
  ProductTitle,
  ProductPrice,
  ProductStock,
  ProductInformation
} from "../styles";
import { Button } from "../../../components";
export default ({ products, listProducts, addItemToCart }: any) => {
  return (
    <Products>
      {products?.map(
        ({
          id,
          isSale,
          name,
          sku,
          currency,
          unitPrice,
          image,
          isAvailable
        }: any) => (
          <Product key={sku}>
            <ProductMeta>
              <ProductImage src={image} alt={name} />
              <ProductInformation>
                <ProductTitle>
                  <Link to={`/products/${id}`}>{name}</Link>
                </ProductTitle>
                <ProductPrice>
                  {currency} {unitPrice}
                </ProductPrice>
                <ProductStock>
                  {isAvailable ? "In Stock" : "Out Of Stock"}
                </ProductStock>
              </ProductInformation>
            </ProductMeta>
            <ProductActions>
              <Button
                color="secondary"
                onClick={() =>
                  addItemToCart({
                    id,
                    qty: 1,
                    unitPrice,
                    image,
                    name,
                    currency,
                    isAvailable
                  })
                }
              >
                add to cart
              </Button>
              <Button color="primary">
                <Link to={`/products/${id}`}>view</Link>
              </Button>
            </ProductActions>
          </Product>
        )
      )}
    </Products>
  );
};
