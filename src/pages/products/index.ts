import React from 'react';

export const Products = React.lazy(() => import('./products'));
