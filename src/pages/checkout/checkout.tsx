import React from "react";
import { useCart } from "../../contexts/cart.context";
import Cart from "./cart/cart";
import { Wrapper, NoItems, EmptyCard, EmptyText } from "./cart/styles";
import { Button } from "../../components";
import { Link } from "react-router-dom";

export const EmptyCart = () => (
  <NoItems>
    <EmptyCard>
      <EmptyText>Your shopping cart is empty</EmptyText>
      <Link to="/products">
        <Button color="secondary">Continue Shopping</Button>
      </Link>
    </EmptyCard>
  </NoItems>
);
export default () => {
  const {
    items,
    total,
    totalPrice,
    removeItemFromCart,
    updateCartItem
  } = useCart();

  if (total < 1) return <EmptyCart />;

  return (
    <Wrapper>
      <Cart
        items={items}
        total={total}
        removeItemFromCart={removeItemFromCart}
        totalPrice={totalPrice}
        addItemToCart={updateCartItem}
      />
    </Wrapper>
  );
};
