import React from 'react';

export const Checkout = React.lazy(() => import('./checkout'));
