import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  background: ${({ color, theme }) => theme.palette[`${color}`] || "none"};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    min-height: 110vh;
  }
`;

export const NoItems = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 25px;
`;

export const EmptyCard = styled.div`
  width: 50%;
  padding: 50px;
  display: flex;
  flex-direction: column;
  align-items: center;
  -webkit-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0px 0px 13px 3px ${({ theme }) => theme.palette.cardBorderColor};
  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 95%;
  }
`;

export const EmptyText = styled.div`
  font-size: 2rem;
  font-weight: 600px;
  margin: 25px;
`;

export const Cart = styled.div`
  width: 75%;
  display: flex;
  flex-direction: row;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 100%;
  }
`;

export const Items = styled.div`
  width: 100%;
  margin-right: 30px;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    margin-right: 0;
    margin-bottom: 150px;
  }
`;
export const Item = styled.div`
  width: 100%;
  display: flex;
  margin: 25px 0;
  padding: 25px;
  -webkit-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0px 0px 13px 3px ${({ theme }) => theme.palette.cardBorderColor};

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    flex-direction: column;
  }
`;

export const ItemImage = styled.img`
  width: 200px;
`;

export const ItemMeta = styled.div`
  width: 100%;
  margin: 0 10px;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    margin: 10px 0px;
  }
`;
export const ItemStock = styled.div`
  font-size: 1rem;
  color: ${({ theme }) => theme.palette.textColorDark};
`;
export const ItemTitle = styled.div`
  font-size: 1.4rem;
  font-weight: 600;
`;

export const ItemPrice = styled.div`
  font-size: 1.4rem;
  font-weight: 500;
`;

export const Summary = styled.div`
  margin-top: 25px;
  padding: 25px;
  height: 200px;
  width: 300px;
  display: inline-flex;
  flex-direction: column;
  -webkit-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0px 0px 13px 3px ${({ theme }) => theme.palette.cardBorderColor};

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 100%;
    height: auto;
    position: fixed;
    bottom: 0;
    background: #fff;
  }
`;

export const Actions = styled.div`
  width: 35%;
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;

  button {
    width: 100%;
  }

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 100%;
  }
`;

export const ProductPrice = styled.div`
  font-size: 1.6rem;
  font-weight: 500;
  margin: 1rem 0;
  color: ${({ theme }) => theme.palette.secondary};
`;
