import React from "react";
import { Link } from "react-router-dom";
import {
  Cart,
  Items,
  Item,
  ItemImage,
  ItemMeta,
  ItemTitle,
  ItemPrice,
  ItemStock,
  Summary,
  Actions,
  ProductPrice
} from "./styles";
import { Spacer, DropDown, Button } from "../../../components";

export default ({
  items,
  total,
  removeItemFromCart,
  totalPrice = 0,
  addItemToCart
}: any) => {
  return (
    <Cart>
      <Items>
        {items?.map(
          ({ id, unitPrice, name, image, qty, currency, isAvailable }: any) => (
            <Item key={id}>
              <ItemImage src={image} alt={name} />
              <ItemMeta>
                <ItemTitle>
                  <Link to={`/products/${id}`}>{name}</Link>
                </ItemTitle>
                <ItemStock>
                  {isAvailable ? "in stock" : "out of stock"}
                </ItemStock>
              </ItemMeta>
              <Actions>
                <ItemPrice>{`${currency} ${unitPrice * qty}`}</ItemPrice>
                <Spacer space={5} />
                <DropDown
                  label="qty"
                  options={["1", "2", "3", "4", "5", "6", "7", "8", "9", "10+"]}
                  onSelect={addItemToCart}
                  item={{ id, unitPrice, name, image, qty, isAvailable }}
                  qty={`${qty}`}
                />
                <Spacer space={5} />
                <Button color="error" onClick={() => removeItemFromCart(id)}>
                  remove item
                </Button>
              </Actions>
            </Item>
          )
        )}
      </Items>

      <Summary>
        TOTAL: ({total} items)
        <ProductPrice>ZAR {totalPrice}</ProductPrice>
        <Button color="secondary">proceed to checkout</Button>
      </Summary>
    </Cart>
  );
};
