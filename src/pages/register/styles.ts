import styled from "styled-components";

export const Form = styled.form`
  width: 400px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 25px;
  -webkit-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0px 0px 13px 3px
    ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0px 0px 13px 3px ${({ theme }) => theme.palette.cardBorderColor};
  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    width: 95%;
  }
`;

export const FormHeader = styled.h1``;

export const FormGroup = styled.div`
  width: 100%;
  margin: 5px 0;
`;
export const FormLabel = styled.label`
  font-size: 1.2rem;
  font-weight: 500;
`;

export const FormError = styled.div`
  color: ${({ theme }) => theme.palette.error};
  padding: 5px 0;
`;

export const Input = styled.input`
  width: 100%;
  padding: 10px;
  margin: 10px 0;
  -webkit-box-shadow: 0 1px 16px 0
    ${({ theme }) => theme.palette.cardBorderColor};
  -moz-box-shadow: 0 1px 16px 0 ${({ theme }) => theme.palette.cardBorderColor};
  box-shadow: 0 1px 16px 0 ${({ theme }) => theme.palette.cardBorderColor};
  outline: none;
  border: none;

  :focus {
    outline: none;
    border: none;
  }
`;
