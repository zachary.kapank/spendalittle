import React, { useState } from "react";
import { Formik, Field } from "formik";
import { useHistory } from "react-router-dom";
import { object, string, ref } from "yup";
import { useAuth } from "../../contexts/auth.context";
import { Loader, Button, Spacer } from "../../components";
import {
  Form,
  FormHeader,
  FormGroup,
  FormLabel,
  FormError,
  Input
} from "./styles";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";

const RegistrationSchema = object().shape({
  firstName: string().required("Required"),
  lastName: string().required("Required"),
  email: string()
    .email("Invalid email")
    .required("Required"),
  password: string().required("Required"),
  repeatPassword: string()
    .required("Required")
    .oneOf([ref("password"), ""], "Passwords must match"),
  location: string().required("Required")
});

const Registration = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { setAuthenticatedUser } = useAuth();

  const handleSubmit = (values: any) => {
    setLoading(true);
    const { firstName, lastName, email, location } = values;

    setAuthenticatedUser({ firstName, lastName, email, location });

    setLoading(false);

    history.push("/products");
  };

  return loading ? (
    <Loader type="Grid" />
  ) : (
    <div
      style={{
        width: "100%",
        margin: "25px 0",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          repeatPassword: "",
          location: ""
        }}
        onSubmit={handleSubmit}
        validationSchema={RegistrationSchema}
      >
        {({ handleChange, handleSubmit, errors, touched }) => (
          <Form onSubmit={handleSubmit}>
            <FormHeader>Register</FormHeader>
            <FormGroup>
              <FormLabel>First Name</FormLabel>
              <Input
                name="firstName"
                onChange={handleChange}
                placeholder="First Name"
                type="text"
              />
              {errors.firstName && <FormError>{errors.firstName}</FormError>}
            </FormGroup>
            <FormGroup>
              <FormLabel>Last Name</FormLabel>
              <Input
                name="lastName"
                onChange={handleChange}
                placeholder="Last Name"
                type="text"
              />
              {errors.lastName && <FormError>{errors.lastName}</FormError>}
            </FormGroup>
            <FormGroup>
              <FormLabel>Email</FormLabel>
              <Input
                name="email"
                onChange={handleChange}
                placeholder="Email"
                type="email"
              />
              {errors.email && <FormError>{errors.email}</FormError>}
            </FormGroup>
            <FormGroup>
              <FormLabel>Password</FormLabel>
              <Input
                name="password"
                type="password"
                onChange={handleChange}
                placeholder="Password"
              />
              {errors.password && <FormError>{errors.password}</FormError>}
            </FormGroup>
            <FormGroup>
              <FormLabel>Repeat Password</FormLabel>
              <Input
                name="repeatPassword"
                type="password"
                onChange={handleChange}
                placeholder="Confirm Password"
              />
              {errors.repeatPassword && (
                <FormError>{errors.repeatPassword}</FormError>
              )}
            </FormGroup>
            <Field name="location">
              {({ form: { errors, setFieldValue }, meta }: any) => (
                <FormGroup>
                  <FormLabel>Location</FormLabel>
                  <GooglePlacesAutocomplete
                    autocompletionRequest={{
                      componentRestrictions: {
                        country: "ZA"
                      }
                    }}
                    onSelect={selection => {
                      setFieldValue("location", selection.description);
                    }}
                  />
                  {meta.error && <FormError>{meta.error}</FormError>}
                </FormGroup>
              )}
            </Field>
            <Spacer space={10} />
            <Button type="submit" color="secondary">
              Sign Up
            </Button>
            <Spacer space={10} />
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Registration;
