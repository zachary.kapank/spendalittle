import React, { FC, useEffect, useState } from "react";

export interface IAuthenticatedUserContext {
  firstName: string;
  lastName: string;
  email: string;
  location: string;
}

interface IProps {
  children: any;
}

interface IState {
  authenticated: boolean;
  authenticatedUser: IAuthenticatedUserContext;
  setAuthenticatedUser: (userPayload: any) => void;
  unsetAuthenticatedUser: () => void;
}

export const DEFAULT_STATE: IState = {
  authenticated: false,
  authenticatedUser: {
    firstName: "",
    lastName: "",
    email: "",
    location: ""
  },
  setAuthenticatedUser: (userPayload: any) => {},
  unsetAuthenticatedUser: () => {}
};

const Provider: FC<IProps> = ({ children }) => {
  const [state, setState] = useState(DEFAULT_STATE);

  useEffect(() => {
    const user: any = JSON.parse(
      localStorage.getItem("authenticatedUser") as any
    );
    if (user) {
      setState({
        ...DEFAULT_STATE,
        authenticated: true,
        authenticatedUser: { ...user }
      });
    }
  }, [setState]);

  const unsetAuthenticatedUser = () => {
    localStorage.removeItem("authenticatedUser");

    setState(DEFAULT_STATE);
  };

  const setAuthenticatedUser = (userPayload: any) => {
    localStorage.setItem("authenticatedUser", JSON.stringify(userPayload));

    setState({
      ...state,
      authenticated: true,
      authenticatedUser: { ...userPayload }
    });
  };

  return (
    <AuthenticatedUserContext.Provider
      value={{
        ...state,
        authenticatedUser: state.authenticatedUser,
        setAuthenticatedUser,
        unsetAuthenticatedUser
      }}
    >
      {children}
    </AuthenticatedUserContext.Provider>
  );
};

export default Provider;
export const AuthenticatedUserContext = React.createContext(DEFAULT_STATE);
export const useAuth = () => React.useContext(AuthenticatedUserContext);
