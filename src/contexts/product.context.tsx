import React, { createContext } from "react";
import {
  getProductById,
  getProducts,
  getProductByTerm
} from "../dataSources/fakeData";
interface IProps {
  children: any;
}

type Product = {
  id: number;
  isSale: boolean;
  unitPrice: number;
  sku: string;
  image: string;
  currency: string;
  description: string;
  isAvailable: boolean;
  name: boolean;
};

const DEFAULT_STATE = {
  product: null,
  products: [],
  pageLimit: 7,
  getProduct: (id: number) => {},
  listProducts: (term: string, offset: number, limit: number) => [],
  search: (term: string) => []
};

interface IProductContext {
  product: Product;
  products: Product[];
  pageLimit: number;
  getProduct: (id: number) => Product;
  listProducts: (offset: number, limit: number, term?: string) => Product[];
  search: () => Product[];
}

export const ProductContext = createContext({});

class Provider extends React.Component<IProps> {
  state = DEFAULT_STATE;

  public componentDidMount() {
    try {
      this.setState({
        ...this.state,
        products: getProducts(0, this.state.pageLimit, undefined)
      });
    } catch (e) {
      return e;
    }
  }

  getProduct = (id: number) => {
    this.setState(prevState => ({
      ...prevState,
      product: getProductById(id)
    }));
  };

  search = (term: string) => {
    this.setState(prevState => ({
      ...prevState,
      products: getProductByTerm(term)
    }));
  };

  listProducts = (offset: number, limit: number, term?: string) => {
    this.setState(prevState => ({
      ...prevState,
      products: getProducts(offset, limit, term)
    }));
  };

  render() {
    return (
      <ProductContext.Provider
        value={{
          ...this.state,
          getProduct: this.getProduct,
          listProducts: this.listProducts,
          search: this.search
        }}
      >
        {this.props.children}
      </ProductContext.Provider>
    );
  }
}

export default Provider;

export const useProduct = () =>
  React.useContext(ProductContext) as IProductContext;
