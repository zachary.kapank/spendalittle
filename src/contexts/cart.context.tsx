import React, { createContext } from "react";
import { addToCart, removeItem, updateCart } from "../dataSources/cart";

interface IProps {
  children: any;
}

type CartItem = {
  id: number;
  qty: number;
  unitPrice: number;
  currency: string;
  image: string;
  name: string;
  isAvailable: boolean;
};

interface ICart {
  total: number;
  totalPrice: number;
  items: CartItem[];
}

const DEFAULT_STATE = {
  total: 0,
  totalPrice: 0,
  items: [],
  addItemToCart: (item: CartItem) => {},
  updateCartItem: (item: CartItem) => {},
  removeItemFromCart: (id: number) => {}
};

export const CartContext = createContext(DEFAULT_STATE);

class Provider extends React.Component<IProps, {}> {
  state = DEFAULT_STATE;

  public async componentDidMount() {
    try {
      const cart = JSON.parse(localStorage.getItem("cart") as string);
      if (!cart) {
        localStorage.setItem("cart", JSON.stringify(DEFAULT_STATE));
      } else {
        this.setState({ ...cart });
      }
    } catch (e) {
      return e;
    }
  }

  addItemToCart = ({
    id,
    qty = 1,
    unitPrice,
    currency,
    image,
    name,
    isAvailable
  }: CartItem) => {
    let cart = JSON.parse(localStorage.getItem("cart") as string) || [];

    const query = addToCart(cart, {
      id,
      qty,
      unitPrice,
      currency,
      image,
      name,
      isAvailable
    });

    this.setState(query);

    localStorage.setItem("cart", JSON.stringify(query));
  };

  updateCartItem = ({
    id,
    qty,
    unitPrice,
    currency,
    image,
    name,
    isAvailable
  }: CartItem) => {
    let cart = JSON.parse(localStorage.getItem("cart") as string) || [];

    const query = updateCart(cart, {
      id,
      qty,
      unitPrice,
      currency,
      image,
      name,
      isAvailable
    });

    this.setState(query);

    localStorage.setItem("cart", JSON.stringify(query));
  };

  removeItemFromCart = (id: number) => {
    let cart = JSON.parse(localStorage.getItem("cart") as string);
    const query = removeItem(cart, id);
    this.setState(query);
    localStorage.setItem("cart", JSON.stringify(query));
  };

  render() {
    return (
      <CartContext.Provider
        value={{
          ...this.state,
          addItemToCart: this.addItemToCart,
          removeItemFromCart: this.removeItemFromCart,
          updateCartItem: this.updateCartItem
        }}
      >
        {this.props.children}
      </CartContext.Provider>
    );
  }
}

export default Provider;

export const useCart = () => React.useContext(CartContext);
