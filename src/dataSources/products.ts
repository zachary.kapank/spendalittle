export const getProductById = (id: number, products: any) => {
	return new Promise((resolve, reject) => {
		const product = products.find((item: any) => item.id === id);
		if (!product) reject({ message: 'product does not exist' });
		resolve(product);
	});
};

export const getProducts = (products: any) => {
	return new Promise((resolve, reject) => {
		const list = products;
		if (!list?.length) reject({ message: 'No products available' });
		resolve(list);
	});
};
