export const addToCart = ({ items }: any, item: any) => {
  const itemExists = items?.some((e: any) => e.id === item.id);
  let updatedCart = [];

  if (itemExists) {
    updatedCart = items?.map((cartItem: any) => {
      if (cartItem.id === item.id) {
        cartItem.qty = cartItem.qty + item.qty;
      }
      return cartItem;
    });
  } else {
    updatedCart = [...items, item];
  }

  const [total, totalPrice] = updateTotals(updatedCart);

  return { total, totalPrice, items: updatedCart };
};

export const updateCart = ({ items }: any, item: any) => {
  const updatedCart = items?.map((cartItem: any) => {
    if (cartItem.id === item.id) {
      cartItem.qty = item.qty;
    }
    return cartItem;
  });
  const [total, totalPrice] = updateTotals(updatedCart);

  return { total, totalPrice, items: updatedCart };
};

export const removeItem = (cart: any, id: number) => {
  const updatedCart = cart?.items?.filter((item: any) => item.id !== id);
  const [total, totalPrice] = updateTotals(updatedCart);

  return { total, totalPrice, items: updatedCart };
};

const updateTotals = (items: any) => {
  let total: number = 0,
    totalPrice: number = 0;

  items?.forEach((item: any) => {
    total += item.qty;
    totalPrice += item.unitPrice * item.qty;
  });

  return [total, totalPrice];
};
