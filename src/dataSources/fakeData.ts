import {
  awayShirt,
  redZipTop,
  contrastHoody,
  offWhiteHoody,
  redAndWhiteWatch,
  retroBabyJumper,
  kidsBronxBeanie,
  programmeHistoryBook,
  bambooMug
} from "./images";

const products = [
  {
    id: 1,
    isSale: true,
    unitPrice: 930,
    sku: "SKU0009",
    image: awayShirt,
    currency: "ZAR",
    description:
      "The official Arsenal Adult 19/20 Short Sleeved Third Shirt has been created by adidas for our fans offering a looser fit across the chest and shoulders than the one worn by the players on the pitch.",
    isAvailable: true,
    name: "Arsenal Adult 19/20 Third Shirt"
  },
  {
    id: 2,
    isSale: true,
    unitPrice: 555,
    sku: "SKU0002",
    image: redZipTop,
    currency: "ZAR",
    description:
      "A classic leisure 1/4 zip top featuring a simple Arsenal crest to the chest. Comfortable pullover for casual wear.",
    isAvailable: false,
    name: "Arsenal Leisure Classic Red 1/4 Zip Top"
  },
  {
    id: 3,
    isSale: true,
    unitPrice: 555,
    sku: "SKU3301",
    image: redZipTop,
    currency: "ZAR",
    description:
      "The Arsenal Since 1886 Splatter T-Shirt can be mixed and matched with items from our Since 1886 collection.",
    isAvailable: true,
    name: "Arsenal Since 1886 Splatter T-Shirt"
  },
  {
    id: 4,
    isSale: true,
    unitPrice: 1000,
    sku: "SKU3207",
    image: contrastHoody,
    currency: "ZAR",
    description:
      "This Arsenal Women's 1886 Contrast Panel Hoody is a worthy addition to our Since 1886 Collection. The hoody is black with a contrast cream upper body featuring large AFC lettering and a round Arsenal brand patch to the left sleeve. The fixed hood has a draw-cord and there is a large kangaroo pocket at the front.",
    isAvailable: true,
    name: "Arsenal Womens Since 1886 Contrast Hoody Cream"
  },
  {
    id: 5,
    isSale: true,
    unitPrice: 1000,
    sku: "SKU3907",
    image: offWhiteHoody,
    currency: "ZAR",
    description:
      "This Arsenal Women's 1886 Contrast Panel Hoody is a worthy addition to our Since 1886 Collection. The hoody is black with a contrast cream upper body featuring large AFC lettering and a round Arsenal brand patch to the left sleeve. The fixed hood has a draw-cord and there is a large kangaroo pocket at the front.",
    isAvailable: true,
    name: "Arsenal Womens Since 1886 Hoody"
  },
  {
    id: 6,
    isSale: true,
    unitPrice: 555,
    sku: "SKU3107",
    image: redAndWhiteWatch,
    currency: "ZAR",
    description:
      "This Arsenal Stripe watch has a round stainless steel case fitted with a burgundy nylon strap with silver stripe. It has a white dial with gold tone detailing, and comes complete with a watch box.",
    isAvailable: true,
    name: "Arsenal Burgundy Stripe Dial Watch"
  },
  {
    id: 7,
    isSale: true,
    unitPrice: 335,
    sku: "SKU3397",
    image: retroBabyJumper,
    currency: "ZAR",
    description:
      "This Arsenal Stripe watch has a round stainless steel case fitted with a burgundy nylon strap with silver stripe. It has a white dial with gold tone detailing, and comes complete with a watch box.",
    isAvailable: true,
    name: "Arsenal Baby Retro 1970s Sleepsuit"
  },
  {
    id: 8,
    isSale: true,
    unitPrice: 225,
    sku: "SKU3357",
    image: kidsBronxBeanie,
    currency: "ZAR",
    description: "Keep you little ones warm with this red bronx hat.",
    isAvailable: true,
    name: "Arsenal Kids Bronx Hat"
  },
  {
    id: 9,
    isSale: true,
    unitPrice: 115,
    sku: "SKU3184",
    image: programmeHistoryBook,
    currency: "ZAR",
    description:
      "A History Of The Arsenal Programme Representing the class and grandeur of Arsenal Football Club throughout its history, the official matchday programme has been a staple of matchday since the club’s early beginnings, late in the 19th century.",
    isAvailable: true,
    name: "Arsenal Programme History Book"
  },
  {
    id: 10,
    isSale: true,
    unitPrice: 445,
    sku: "SKU3538",
    image: bambooMug,
    currency: "ZAR",
    description:
      "Keep your drinks hot with this double insulated, eco-friendly bamboo mug. Prevent hand burns with the outer wooden surface and handle. Cover your on-the-go coffee with the plastic lid and keep hotter for even longer.",
    isAvailable: true,
    name: "Arsenal Bamboo Thermos Mug"
  }
];

export const getProducts = (offset: number, limit: number, term?: string) => {
  if (term)
    return products
      .filter((item: any) =>
        item.name.toLowerCase().includes(term.toLowerCase())
      )
      .slice(offset, offset + limit);
  return products.slice(offset, offset + limit);
};

export const getProductById = (id: number) => {
  return products.find((item: any) => item.id === id);
};

export const getProductByTerm = (term: string) => {
  if (!term) return products;
};
