import React, { FC } from 'react';
import { Wrapper } from './styles';

export interface IProps {
	space?: number;
	horizontal?: boolean;
}

const Spacer: FC<IProps> = ({ children, ...rest }) => (
	<Wrapper {...rest}>{children}</Wrapper>
);

export default Spacer;
