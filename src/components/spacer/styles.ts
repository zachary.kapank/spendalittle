import styled from 'styled-components';

export const Wrapper = styled.div`
	margin: ${({ space, horizontal }: any) =>
		horizontal ? `0px ${space}px` : `${space}px 0px`};
`;
