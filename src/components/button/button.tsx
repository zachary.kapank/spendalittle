import styled from "styled-components";

export const Button = styled.button<{ width?: string }>`
  width: ${({ width }) => width};
  padding: 1rem;
  border: none;
  outline: none;
  cursor: pointer;
  font-weight: 600;
  font-size: 1.2rem;
  text-align: center;
  color: #fff;
  background: ${({ color, disabled, theme }) =>
    disabled ? "rgba(0,0,0,0.2)" : theme.palette[`${color}`]};
  &:hover {
    transition: 0.15s ease-in-out;
  }
`;
