import React, { useState } from "react";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";

export default ({ onSelect, options, label, qty, item }: any) => {
  const [selected, setSelected] = useState();

  if (selected === 10) {
    return (
      <input
        defaultValue={10}
        onChange={e => onSelect({ ...item, qty: parseInt(e.target.value) })}
      />
    );
  }
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignContent: "center"
      }}
    >
      <div style={{ alignSelf: "center", marginRight: "1.2rem" }}>Qty:</div>
      <Dropdown
        options={options}
        onChange={value => {
          if (value.value === "10+") {
            setSelected(10);
            onSelect({ ...item, qty: 10 });
          } else {
            onSelect({ ...item, qty: parseInt(value.value) });
          }
        }}
        value={qty}
        placeholder={label}
      />
    </div>
  );
};
