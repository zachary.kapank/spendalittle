import React, { FunctionComponent } from 'react';
import { Route, RouteProps, Switch } from 'react-router-dom';

export interface IRouteProps extends RouteProps {
	exact?: boolean;
	path: any;
	title: string;
	Component: any;
	hiddenFromRender?: boolean | null;
}

interface IProps {
	routes: IRouteProps[];
}

/**
 * @render react
 * @name Routes component
 * @description Routes component.
 * @example
 * <Routes
 *  routes={[
 *    {
 *      exact: true,
 *      path: '/',
 *      Component: Product,
 *      title: 'Product'
 *    }
 *  ]}
 * />
 */

const Routes: FunctionComponent<IProps> = ({ routes }: IProps) => (
	<Switch>
		{routes.map((links, index: number) => {
			const { path, exact, Component } = links;

			return (
				<Route {...(exact ? { exact } : {})} key={index} path={path}>
					<Component />
				</Route>
			);
		})}
	</Switch>
);

export default Routes;
