import { Checkout, Product, Products, Register } from '../pages';

const getRoutesForUser = () => {
	return [
		{
			exact: true,
			path: '/products',
			Component: Products,
			title: 'products',
			hiddenFromRender: false,
		},
		{
			exact: true,
			path: '/products/:productId',
			Component: Product,
			title: 'product',
			hiddenFromRender: false,
		},
		{
			exact: true,
			path: '/checkout',
			Component: Checkout,
			title: 'checkout',
			hiddenFromRender: false,
		},
		{
			exact: true,
			path: '/register',
			Component: Register,
			title: 'register',
			hiddenFromRender: false,
		},
	];
};

export default getRoutesForUser;
