import { createGlobalStyle } from "styled-components";

export interface IPalette {
  [key: string]: string;
  primary: string;
  secondary: string;
  bodyBackground: string;
  cardBackground: string;
  cardBorderColor: string;
  linkColor: string;
  textColorDark: string;
  textColorMedium: string;
  textColorLight: string;
  dividerColor: string;
  cardTitleColor: string;
  backButtonColor: string;
  white: string;
  offWhite: string;
  blue: string;
  warning: string;
  error: string;
  success: string;
}

export interface ITheme {
  theme: {
    breakpoints: {
      small: string;
      medium: string;
      large: string;
      xLarge: string;
    };
    fontFamily: string;
    fontSize: string[];
    palette: IPalette;
    spacing: number[];
  };
}

export const THEME = {
  breakpoints: {
    small: "768px",
    medium: "1024px",
    large: "1440px",
    xLarge: "1920px"
  },
  fontFamily: `'Poppins',-apple-system,BlinkMacSystemFont,Segoe UI
  ,Roboto,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif`,
  fontSize: [
    "11px",
    "13px",
    "14px",
    "15px",
    "18px",
    "20px",
    "22px",
    "25px",
    "40px"
  ],
  palette: {
    primary: "#FFF",
    secondary: "#FB6A02",
    bodyBackground: "#F0F3F9",
    cardBackground: "#FFFFFF",
    cardBorderColor: "#CACACA",
    linkColor: "#057AFF",
    textColorDark: "#545665",
    textColorMedium: "#7c7d87",
    textColorLight: "#9397ab",
    dividerColor: "#d6dbe6",
    cardTitleColor: "#33353d",
    backButtonColor: "#1b154a",
    white: "#FFFFFF",
    offWhite: "#fafafa",
    blue: "#1479f6",
    warning: "#f39049",
    error: "#ff0000",
    success: "#4cd964"
  },
  spacing: [5, 10, 20, 30, 35, 40, 80]
};

export default createGlobalStyle`
  * {
    -webkit-tap-highlight-color: transparent;
    box-sizing: border-box;
  }

  *::-webkit-scrollbar {
    width: 8px !important;
    height: 2px !important
  }

  *::-webkit-scrollbar-thumb {
    background-color: rgba(0,0,0,0.2)
  }

  *::-webkit-scrollbar-track {
    background: rgba(255,255,255,0.08)
  }

  html,
  body {
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    background-color: ${({ theme }: ITheme) => theme.palette.offWhite};
    font-feature-settings: 'liga', 'kern' 1;
    font-kerning: normal;
    font-size: ${({ theme }: ITheme) => theme.fontSize[1]};
    font-weight: 400;
    height: 100%;
    margin: 0;
    padding: 0;
    text-rendering: optimizeLegibility;
    width: 100%;
  }

  html {
    overflow-x: hidden;
  }

  body {
    font-family: ${({ theme }: ITheme) => theme.fontFamily};
  }

  hr {
    background-color: ${({ theme }: ITheme) => theme.palette.cardBorderColor};
    border: none;
    height: 1px;
  }

  .sr-only {
    height: 0;
    opacity: 0;
    position: absolute;
  }

  [disabled] {
    opacity: 0.5;
  }

  [href] {
    color: ${({ theme }: ITheme) => theme.palette.linkColor};
    text-decoration: none;
  }

  .c-btn--back {
    align-items: center;
    background-color: transparent;
    display: flex;
    color: ${({ theme }: ITheme) => theme.palette.backButtonColor};
    padding-left: 0;

    svg {
      margin-right: ${({ theme }: ITheme) => theme.spacing[2]};
    }
  }

  .Toastify__toast--success{
    background-color: ${({ theme }: ITheme) => theme.palette.blue}};

  .paginate {
    list-style: none;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .page{
      padding: 10px 15px;
      color: ${({ theme }: ITheme) => theme.palette.secondary};
      font-weight: 500;
      font-size: 1.4rem;
      cursor: pointer;
  }
    .next, .previous {
      padding: 0;
      background: transparent;
      outline: none;
           cursor: pointer;
  
    }
    .activePagination {
      padding: 10px 15px;
      font-weight: 500;
      font-size: 1.2rem;
       background: ${({ theme }: ITheme) => theme.palette.secondary};
       color: ${({ theme }: ITheme) => theme.palette.white};
    }
    .google-places-autocomplete {
  width: 100%;
  position: relative;
}

.google-places-autocomplete__input {
  width: 100%;
  padding: 10px;
  border: none;
  margin: 10px 0;
  box-shadow: 0 1px 16px 0 rgba(0, 0, 0, 0.09);
}

.google-places-autocomplete__input:active,
.google-places-autocomplete__input:focus,
.google-places-autocomplete__input:hover {
  outline: none;
  border: none;
}

.google-places-autocomplete__suggestions-container {
  background: white;
  border-radius: 0 0 5px 5px;
  color: black;
  position: absolute;
  width: 100%;
  z-index: 2;
  box-shadow: 0 1px 16px 0 rgba(0, 0, 0, 0.09);
}

.google-places-autocomplete__suggestion {
  font-size: 1rem;
  text-align: left;
  padding: 10px;
}

.google-places-autocomplete__suggestion--active {
  background: #e0e3e7;
}

`;
