import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import GlobalStyles, { THEME } from "../globalStyles";
import ProductProvider from "../contexts/product.context";
import CartProvider from "../contexts/cart.context";
import AuthenticatedUserProvider from "../contexts/auth.context";
import { Routes, Loader } from "../components";
import getRoutesForUser from "../utils/getRoutesForUser";
import Navbar from "./Navbar";

const routes = getRoutesForUser();

export default () => {
  return (
    <ThemeProvider theme={THEME}>
      <GlobalStyles />
      <Router>
        <AuthenticatedUserProvider>
          <ProductProvider>
            <CartProvider>
              <Navbar />
              <React.Suspense
                fallback={
                  <Loader type={"Grid"} color={THEME.palette.secondary} />
                }
              >
                <Routes routes={routes} />
              </React.Suspense>
            </CartProvider>
          </ProductProvider>
        </AuthenticatedUserProvider>
      </Router>
    </ThemeProvider>
  );
};
