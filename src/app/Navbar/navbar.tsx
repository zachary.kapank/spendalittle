import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Wrapper,
  Nav,
  Logo,
  Menu,
  Item,
  Burger,
  CartItem,
  MobileMenu,
  MobileMenuItems,
  MobileMenuItem,
  BurgerClose
} from "./styles";
import { useCart } from "../../contexts/cart.context";
//Cart
import { ShoppingCart } from "@styled-icons/heroicons-solid/ShoppingCart";
import { useAuth } from "../../contexts/auth.context";

export default () => {
  const { total } = useCart();
  const [show, setShow] = useState(false);
  const {
    authenticated,
    authenticatedUser,
    unsetAuthenticatedUser
  } = useAuth();
  return (
    <Wrapper>
      <Nav>
        <Burger onClick={() => setShow(!show)} />
        <Logo>
          <Link to="/">spendalittle</Link>
        </Logo>
        <Menu>
          <Item>About Us</Item>
          <Item>Our Services</Item>
          <Item>Jobs</Item>
          <Item>Contact Us</Item>
          <Item>
            <Link to="/products">Products</Link>
          </Item>
          {authenticated ? (
            <>
              <Item rightLine>Hello, {authenticatedUser.firstName}</Item>
              <Item onClick={() => unsetAuthenticatedUser()}>
                <Link to="#">Sign Out</Link>
              </Item>
            </>
          ) : (
            <>
              <Item rightLine>Login</Item>
              <Item>
                <Link to="/register">Register</Link>
              </Item>
            </>
          )}
          <CartItem round contained>
            <Link to="/checkout">
              <ShoppingCart color="#fff" size="24px" />
              <span>{total > 0 && total}</span>
            </Link>
          </CartItem>
        </Menu>
      </Nav>
      {show && (
        <MobileMenu>
          <BurgerClose onClick={() => setShow(!show)} />
          <MobileMenuItems>
            <MobileMenuItem onClick={() => setShow(!show)}>Home</MobileMenuItem>
            <MobileMenuItem onClick={() => setShow(!show)}>
              About Us
            </MobileMenuItem>
            <MobileMenuItem onClick={() => setShow(!show)}>
              Our Services
            </MobileMenuItem>
            <MobileMenuItem onClick={() => setShow(!show)}>Jobs</MobileMenuItem>
            <MobileMenuItem onClick={() => setShow(!show)}>
              Contact Us
            </MobileMenuItem>
            <MobileMenuItem onClick={() => setShow(!show)}>
              Login
            </MobileMenuItem>
            <MobileMenuItem onClick={() => setShow(!show)}>
              <Link to="/register">Register</Link>
            </MobileMenuItem>
            <MobileMenuItem onClick={() => setShow(!show)}>
              <Link to="/products">Products</Link>
            </MobileMenuItem>
          </MobileMenuItems>
        </MobileMenu>
      )}
    </Wrapper>
  );
};
