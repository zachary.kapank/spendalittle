import styled from "styled-components";
import { MenuAltLeft } from "@styled-icons/boxicons-regular/MenuAltLeft";
import { Cross } from "@styled-icons/entypo/Cross";

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  min-height: 40px;
  background: ${({ theme }) => theme.palette.white};
`;
export const Nav = styled.nav`
  width: 90%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 15px;

  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    bottom: 0;
    width: 100%;
  }
`;

export const Logo = styled.div`
  a {
    font-weight: 700;
    font-size: 3rem;
    color: ${({ theme }) => theme.palette.textColorDark};
  }
`;

export const Menu = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: "center";
`;

export const Item = styled.div<{
  contained?: boolean;
  round?: boolean;
  rightLine?: boolean;
}>`
  font-weight: 500;
  font-size: 1.2rem;
  align-self: center;
  border-right: ${({ rightLine, theme }) =>
    rightLine && `1px solid ${theme.palette.textColorDark}`};
  background: ${({ contained, theme }) =>
    contained ? theme.palette.secondary : "none"};
  padding: 5px 10px;
  color: ${({ theme }) => theme.palette.textColorDark};
  border-radius: ${({ round }) => round && "30px"};
  a {
    color: ${({ theme }) => theme.palette.textColorDark};
  }

  span {
    margin-left: 3px;
    font-weight: 600px;
    color: ${({ theme }) => theme.palette.primary};
  }
  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    display: none;
  }
`;

export const CartItem = styled(Item)`
  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    display: inline;
    width: auto;
  }
`;

export const Burger = styled(MenuAltLeft)`
  height: 55px;
  display: none;
  color: ${({ theme }) => theme.palette.textColorDark};
  @media screen and (max-width: ${({ theme }) => theme.breakpoints.small}) {
    display: inline;
  }
`;
export const BurgerClose = styled(Cross)`
  color: ${({ theme }) => theme.palette.textColorDark};
  height: 65px;
  margin: 12px;
`;

export const MobileMenu = styled.div`
  height: 100vh;
  width: 100vw;
  background: #fff;
  position: absolute;
  z-index: 9;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`;

export const MobileMenuItems = styled.div`
  height: 100%;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const MobileMenuItem = styled.div`
  font-weight: 500;
  font-size: 1.4rem;
  margin: 15px 0;
  -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
  -moz-animation: fadein 1s; /* Firefox < 16 */
  -ms-animation: fadein 1s; /* Internet Explorer */
  -o-animation: fadein 1s; /* Opera < 12.1 */
  animation: fadein 1s;
  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  /* Firefox < 16 */
  @-moz-keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  /* Safari, Chrome and Opera > 12.1 */
  @-webkit-keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  /* Internet Explorer */
  @-ms-keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  /* Opera < 12.1 */
  @-o-keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  a {
    color: ${({ theme }) => theme.palette.textColorDark};
  }
`;
