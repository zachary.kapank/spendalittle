### `yarn`

Installs modules required to run the app

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

### NB - GOOGLE API KEY

To use google auto complete places, please insert a valid API_KEY in the google script inside /public/index.html
